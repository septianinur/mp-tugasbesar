package com.TA.tugasakhir;

import android.content.Context;
import android.database.Cursor;
import android.view.*;
import android.widget.*;

public class RianAdapter extends CursorAdapter {

	@SuppressWarnings("deprecation")
	public RianAdapter(Context context, Cursor c) {
		super(context, c);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View retView = inflater.inflate(R.layout.list_item, parent, false);
		return retView;
	}
	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		TextView nama = (TextView) view.findViewById(R.id.judul);
		nama.setText(cursor.getString(cursor.getColumnIndex("judul")));
		
	}

	
}
