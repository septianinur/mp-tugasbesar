package com.TA.tugasakhir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Menu_huruf extends Activity{

	Button b1, b2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_huruf);

        TextView t1 = (TextView) findViewById(R.id.penjelasan);
        String a = "Perbedaan antara Hiragana dan Katakana yaitu pada penggunaan hurufnya.\n" +
        		"Huruf Hiragana digunakan untuk menulis kata yang berasal dari bahasa Jepang asli\n" +
        		"Sedangkan Katakana digunakan untuk menulis kata-kata yang berasal dari bahasa terapan\n" +
        		"atau bahasa yang diluar dari bahasa Jepang asli\n";
        t1.setText(a);
		
	b1	=	(Button)findViewById(R.id.button1);
	b2	=	(Button)findViewById(R.id.button2);
	
		b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getApplicationContext(), Materi_hiragana.class);
				startActivity(i);
			}
		});
		
		b2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getApplicationContext(), Materi_katakana.class);
				startActivity(i);
				
			}
		});
	}
}
