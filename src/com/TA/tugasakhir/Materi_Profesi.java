package com.TA.tugasakhir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class Materi_Profesi extends Activity implements OnClickListener, OnItemClickListener {
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.materi_isi);
        GridView gridView = (GridView) findViewById(R.id.gridview_followed);
        gridView.setAdapter(new a_profesi(this));
        gridView.setOnItemClickListener(this);
        
        TextView t1 = (TextView) findViewById(R.id.penjelasan);
        String a = "POLA KALIMAT \n (Orang) wa (Nama Profesi) desu\n" +
        		"Contoh :\n Watashi wa isha desu (Pekerjaan saya dokter)";
        t1.setText(a);
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent i = new Intent(this, detail_image_profesi.class);
        Bundle b = new Bundle();
        b.putInt("posisi", position);
        i.putExtras(b);
        startActivity(i);
	}

	@Override
	public void onClick(View arg0) {}
}