package com.TA.tugasakhir;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

public class detail_image_profesi extends Activity{

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.materi_detail);

        int imId = this.getIntent().getExtras().getInt("posisi");
        ImageView iv = (ImageView) findViewById(R.id.singleimage);
        int image = a_profesi.mThumbIds[imId];
        iv.setImageResource(image);

    }
}
