package com.TA.tugasakhir;

import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class a_bb extends BaseAdapter{
Context mContext;
	
	public a_bb(Context c)
    {
        mContext = c;
    }
 
    public static Integer[] mThumbIds =
        {
	    	R.drawable.bb1, R.drawable.bb2, R.drawable.bb3, R.drawable.bb4, R.drawable.bb5,
	        R.drawable.bb6, R.drawable.bb7,R.drawable.bb8, R.drawable.bb9, R.drawable.bb10,
	    	R.drawable.bb11
        };
	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
			if (convertView == null) {
				imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(125, 125));
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				imageView.setPadding(8, 8, 8, 8);
			} else {
				imageView = (ImageView) convertView;
			}
		imageView.setImageResource(mThumbIds[position]);
		return imageView;
	    }
}