package com.TA.tugasakhir;

import android.os.*;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

public class Main_Activity extends Activity {
	
	SQLiteDatabase db;
	Cursor data;
	TextView _id,judul,satu,dua,tiga,empat,lima,enam,tujuh,delapan,sembilan,sepuluh,sebelas,duabelas,tigabelas,empatbelas,limabelas;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitymain);
        
        Database db = new Database(this);
        
        try{
        	db.exec("insert into cakap(_id, judul, subjud1, isi1, isi2, " +
        			"subjud2, isi3, isi4, subjud3, isi5, isi6, " +
        			"subjud4, isi7, isi8, subjud5, isi9, isi10)" +
        			"values(1, 'Menyapa Orang'," +
        			"'Pagi','Ohayou gozaimasu(おはよう　ございます)','Selamat Pagi (katakan ini hingga kira kira pukul 10 pagi)'," +
        			"'Siang','Konnichi wa(こんにちわ)','Selamat Siang (katakan ini dari kira kira pukul 10 pagi hingga 5 sore)'," +
        			"'Malam','Konban wa(こんばんわ)','Selamat malam (katakan ini dari pukul 6 sore hingga tengah malam)'," +
        			"'Tidur','Oyasumi nasai(おやすみなさい)','Selamat tidur'," +
        			"'Sampai Jumpa','Mata ashita(また　あした)','Sampai jumpa besok')");

        	db.exec("insert into cakap(_id, judul, subjud1, isi1, isi2, " +
        			"subjud2, isi3, isi4, subjud3, isi5, isi6, " +
        			"subjud4, isi7, isi8, subjud5, isi9, isi10)" +
        			"values(2, 'Perkenalan'," +
        			"'Izin Memperkenalkan Diri','Jiko shoukai shitemo ii desuka?(じこ　しょうかい　しても　いい　ですか？)','Bolehkah saya memperkenalkan diri?'," +
        			"'Memperkenalkan Diri','Watakushi no namae wa de mente desu(わたくし　の　なまえ　わ　デ　メン手　です)','Nama saya de mente'," +
        			"'Menanyakan Nama','O-namae wa nan desu ka?(おなまえ　わ　なん　ですか？)','Siapakah namamu?'," +
        			"'Meminta dikenalkan pada orang lain','Ano otoko no-hito ni shoukai shite kudasai(あの　おとこ　の　ひと　に　しょうかい　して　ください)','Tolong perkenalkan saya pada pria itu'," +
        			"'Ucapan Terima Kasih','Hajimemashite, douzo yoroshiku(はじめまして、どうぞ　よろしく)','Senang berjumpa dengan anda')");
        	
        	db.exec("insert into cakap(_id, judul, subjud1, isi1, isi2, " +
        			"subjud2, isi3, isi4, subjud3, isi5, isi6, " +
        			"subjud4, isi7, isi8, subjud5, isi9, isi10)" +
        			"values(3, 'Mengajukan pertanyaan'," +
        			"'menanyakan asal daerah','Anata wa chuugoku-jin desu ka? (あなた　は　ちゅうごくじん　ですか？)','Apakah kamu orang china?'," +
        			"'menjawab asal daerah','Iie, watashi wa america-jin desu ka（いいえ、アメリカじん　です。）','Bukan, saya orang amerika'," +
        			"'menanyakan keinginan','Nani o tabetai desu ka?（なに　を　たべたい　ですか？）','Anda mau makan apa?'," +
        			"'menawarkan Makanan','Nihon no ryouri wa dou desuka?（にほん　の　りょうり　は　どう　ですか？）','Apakah anda ingin makan masakan jepang'," +
        			"'menanyakan pendapat','Seiyou ryouri wa dou desuka？（せいよう　りょうり　は　どう　ですか？）','Bagaimana dengan masakan barat?')");
        	
        	db.exec("insert into cakap(_id, judul, subjud1, isi1, isi2, " +
        			"subjud2, isi3, isi4, subjud3, isi5, isi6, " +
        			"subjud4, isi7, isi8, subjud5, isi9, isi10)" +
        			"values(4, 'Pergi ke suatu tempat'," +
        			"'menyatakan keinginan','Konban doko ka ni ikitai desu （こんばん　どこ　か　いきたい　です）','Saya mau pergi ke suatu tempat sore ini'," +
        			"'mengajak pergi','Issho ni ikimasen ka　（いっしょ　に　いきません　か？）','Maukah anda pergi dengan saya'," +
        			"'bertanya tujuan orang','Doko e ikitai no desu ka　（どこ　へ　いきたい　の　ですか？）','Anda mau pergi kemana?'," +
        			"'menyatakan tujuan(tempat)','Sushi ya ni ikitai desu　（すし　や　に　いきたい　です）','Saya ingin pergi ke toko sushi'," +
        			"'mengajak berjalan-jalan','Sanpo shimashou　（さんぽ　しましょう）','Marilah kita berjalan jalan')");
        	
        	db.exec("insert into cakap(_id, judul, subjud1, isi1, isi2, " +
        			"subjud2, isi3, isi4, subjud3, isi5, isi6, " +
        			"subjud4, isi7, isi8, subjud5, isi9, isi10)" +
        			"values(5, 'Mencantumkan Tanggal'," +
        			"'menyatakan rencana kepergian','Ni-juu-ichi-nichi ni oosaka e ikimasu　（に　じゅう　いち　にち　に　おおさか　へ　　いきます）','Kita akan pergi ke osaka pada tanggal 21'," +
        			"'memberitahu tanggal','Ashita wa muika desu　（あした　わ　むいか　です）','Besok adalah tanggal 6'," +
        			"'menyatakan rencana kepulangan','Juu-hachi-nichi ni kaerimasu　（じゅう　はち　にち　に　かえります）','Kita pulang tanggal 18'," +
        			"'memberitahu tanggal','Kyou wa juu-ichi gatsu no ni-juu-hachi-nichi desu　（きょう　わ　じゅう　いち　がつ　の　に　じゅう　はち　にち　です）','Hari ini tanggal 28 november'," +
        			"'menyatakan rencana kepulangan','Rai getsu modorimasu　（らい　げつ　もどります）','Saya akan kembali bulan depan')");
        	
        	db.exec("insert into cakap(_id, judul, subjud1, isi1, isi2, " +
        			"subjud2, isi3, isi4, subjud3, isi5, isi6, " +
        			"subjud4, isi7, isi8, subjud5, isi9, isi10)" +
        			"values(6, 'Menggunakan uang'," +
        			"'menyatakan keadaan','Okane o wasuremashita　（おかね　を　わすれました）','Saya lupa membawa uang'," +
        			"'meminta tukar uang','Kore o komakaku shite kudasai　（これ　を　こまかく　して　ください）','Mohon tukar dengan uang receh (logam)'," +
        			"'meminjam uang','Go sen en kashite kudasai　（ごせん　えん　かして　ください）','Tolong pinjamkan saya 5000 yen'," +
        			"'menyatakan keadaan','Kore wa taka sugimasu　（これ　は　たか　すぎます）','Ini terlampau mahal'," +
        			"'bertanya sesuatu','En o motte imasu ka　（えん　を　もって　いますか）','Apakah anda punya mata uang yen?')");
        	
        	db.exec("insert into cakap(_id, judul, subjud1, isi1, isi2, " +
        			"subjud2, isi3, isi4, subjud3, isi5, isi6, " +
        			"subjud4, isi7, isi8, subjud5, isi9, isi10)" +
        			"values(7, 'ketika berbelanja'," +
        			"'menyatakan keinginan','Sore e misete kudasai（それ　を　みせて　ください）','Perlihatkan (barang) itu'," +
        			"'menyatakan keinginan','Kite mite mo ii desu ka（きて　みて　も　いい　ですか？）','Bolehkan saya mencobanya?'," +
        			"'menyatakan keinginan','O-miyage o kaitai desu（おみやげ　を　かいたい　です）','Saya ingin membeli sebuah bingkisan'," +
        			"'mengajak berbelanja','Ashita kaimono ni ikimashou（あした　かいもの　に　いきましょう）','Besok mari kita berbelanja'," +
        			"'menyatakan rencana','Kyou gogo ni kaimono ni ikitai desu（きょう　ごご　に　かいもの　に　いきたい　です）','Saya ingin berbelanja siang ini')");
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        final Cursor cursor = db.get("select * from cakap");
        final ListView list = (ListView) findViewById(R.id.list);
        	new Handler().post(new Runnable(){
        		@Override
        		public void run(){
        			RianAdapter adapter = new RianAdapter(Main_Activity.this, cursor);
        			list.setAdapter(adapter);
        		}
        	});
        	
        	list.setOnItemClickListener(new OnItemClickListener() {
        		
        		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					Cursor jk = (Cursor) list.getItemAtPosition(position);
					String _id = jk.getString(jk.getColumnIndex("_id"));
					String judul = jk.getString(jk.getColumnIndex("judul"));
					String satu = jk.getString(jk.getColumnIndex("subjud1"));
					String dua = jk.getString(jk.getColumnIndex("isi1"));
					String tiga = jk.getString(jk.getColumnIndex("isi2"));
					String empat = jk.getString(jk.getColumnIndex("subjud2"));
					String lima = jk.getString(jk.getColumnIndex("isi3"));
					String enam = jk.getString(jk.getColumnIndex("isi4"));
					String tujuh = jk.getString(jk.getColumnIndex("subjud3"));
					String delapan = jk.getString(jk.getColumnIndex("isi5"));
					String sembilan = jk.getString(jk.getColumnIndex("isi6"));
					String sepuluh = jk.getString(jk.getColumnIndex("subjud4"));
					String sebelas = jk.getString(jk.getColumnIndex("isi7"));
					String duabelas = jk.getString(jk.getColumnIndex("isi8"));
					String tigabelas = jk.getString(jk.getColumnIndex("subjud5"));
					String empatbelas = jk.getString(jk.getColumnIndex("isi9"));
					String limabelas = jk.getString(jk.getColumnIndex("isi10"));
					Intent intent = new Intent(Main_Activity.this, DetailActivity.class);
					intent.putExtra("a", _id);
					intent.putExtra("b", judul);
					intent.putExtra("c", satu);
					intent.putExtra("d", dua);
					intent.putExtra("e", tiga);
					intent.putExtra("f", empat);
					intent.putExtra("g", lima);
					intent.putExtra("h", enam);
					intent.putExtra("i", tujuh);
					intent.putExtra("j", delapan);
					intent.putExtra("k", sembilan);
					intent.putExtra("l", sepuluh);
					intent.putExtra("m", sebelas);
					intent.putExtra("n", duabelas);
					intent.putExtra("o", tigabelas);
					intent.putExtra("p", empatbelas);
					intent.putExtra("q", limabelas);
					startActivity(intent);
				}
			});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }   
}	