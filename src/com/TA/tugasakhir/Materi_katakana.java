package com.TA.tugasakhir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class Materi_katakana extends Activity implements OnClickListener, OnItemClickListener {
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.materi_isi);
        GridView gridView = (GridView) findViewById(R.id.gridview_followed);
        gridView.setAdapter(new a_katakana(this));
        gridView.setOnItemClickListener(this);
        
        TextView t1 = (TextView) findViewById(R.id.penjelasan);
        String a = "Katakana yaitu salah satu cara penulisan dalam bahasa Jepang " +
        		"yang biasanya dipakai untuk kata kata serapan asing" +
        		"yang biasanya disebut sebagai �tulisan laki-laki�. " +
        		"Hal tersebut karena selain sebagian besar laki-laki pada zaman dulu " +
        		"yang menggunakan cara penulisan bahasa Jepang ini, yaitu cara penulisan " +
        		"bahasa Jepang ini memiliki huruf-huruf yang garis-garisnya tegas " +
        		"(melambangkan sikap laki-laki yang tegas).";
        t1.setText(a);
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent i = new Intent(this, detail_image_katakana.class);
        Bundle b = new Bundle();
        b.putInt("posisi", position);
        i.putExtras(b);
        startActivity(i);
	}

	@Override
	public void onClick(View arg0) {}
}