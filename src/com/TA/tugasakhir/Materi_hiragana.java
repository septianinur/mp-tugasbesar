package com.TA.tugasakhir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class Materi_hiragana extends Activity implements OnClickListener, OnItemClickListener {
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.materi_isi);
        GridView gridView = (GridView) findViewById(R.id.gridview_followed);
        gridView.setAdapter(new a_hiragana(this));
        gridView.setOnItemClickListener(this);
        
        TextView t1 = (TextView) findViewById(R.id.penjelasan);
        String a = "Hiragana yaitu salah satu cara penulisan dalam bahasa Jepang" +
        		"yang biasa digunakan untuk kata kata yang memang asli dari negara ini sendiri" +
        		" yang biasanya cara penulisan ini sering disebut dengan �tulisan perempuan�," +
        		"karena pada zaman dulu kaum perempuan lebih sering menggunakan cara" +
        		"penulisan Hiragana dibandingkan Katakana maupun kanji." +
        		"Selain itu, pada cara penulisan Hiragana, huruf-hurufnya" +
        		"lebih terlihat lemah gemulai (meliuk-liuk) yang melambangkan sikap kaum perempuan" +
        		"yang lemah gemulai.";
        t1.setText(a);
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent i = new Intent(this, detail_image_hiragana.class);
        Bundle b = new Bundle();
        b.putInt("posisi", position);
        i.putExtras(b);
        startActivity(i);
	}

	@Override
	public void onClick(View arg0) {}
}