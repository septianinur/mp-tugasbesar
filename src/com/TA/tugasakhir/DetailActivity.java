package com.TA.tugasakhir;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		TextView satu = (TextView) findViewById(R.id.satu);
		TextView dua = (TextView) findViewById(R.id.dua);
		TextView tiga = (TextView) findViewById(R.id.tiga);
		TextView empat = (TextView) findViewById(R.id.empat);
		TextView lima = (TextView) findViewById(R.id.lima);
		TextView enam = (TextView) findViewById(R.id.enam);
		TextView tujuh = (TextView) findViewById(R.id.tujuh);
		TextView delapan = (TextView) findViewById(R.id.delapan);
		TextView sembilan = (TextView) findViewById(R.id.sembilan);
		TextView sepuluh = (TextView) findViewById(R.id.sepuluh);
		TextView sebelas = (TextView) findViewById(R.id.sebelas);
		TextView duabelas = (TextView) findViewById(R.id.duabelas);
		TextView tigabelas = (TextView) findViewById(R.id.tigabelas);
		TextView empatbelas = (TextView) findViewById(R.id.empatbelas);
		TextView limabelas = (TextView) findViewById(R.id.limabelas);
		
		Bundle Extras = getIntent().getExtras();
		String c,d,e,f,g,h,i,j,k,l,m,n,o,p,q;
		
		c=Extras.getString("c");
		d=Extras.getString("d");
		e=Extras.getString("e");
		f=Extras.getString("f");
		g=Extras.getString("g");
		h=Extras.getString("h");
		i=Extras.getString("i");
		j=Extras.getString("j");
		k=Extras.getString("k");
		l=Extras.getString("l");
		m=Extras.getString("m");
		n=Extras.getString("n");
		o=Extras.getString("o");
		p=Extras.getString("p");
		q=Extras.getString("q");
		
		satu.setText(c);
		dua.setText(d);
		tiga.setText(e);
		empat.setText(f);
		lima.setText(g);
		enam.setText(h);
		tujuh.setText(i);
		delapan.setText(j);
		sembilan.setText(k);
		sepuluh.setText(l);
		sebelas.setText(m);
		duabelas.setText(n);
		tigabelas.setText(o);
		empatbelas.setText(p);
		limabelas.setText(q);
	}

}
