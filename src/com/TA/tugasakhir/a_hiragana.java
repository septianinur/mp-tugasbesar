package com.TA.tugasakhir;

import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class a_hiragana extends BaseAdapter{
Context mContext;
	
	public a_hiragana(Context c)
    {
        mContext = c;
    }
 
    public static Integer[] mThumbIds =
        {
	    	R.drawable.h1, R.drawable.h2, R.drawable.h3, R.drawable.h4, R.drawable.h5,
	        R.drawable.h6, R.drawable.h7,R.drawable.h8, R.drawable.h9, R.drawable.h10,
	    	R.drawable.h11, R.drawable.h12, R.drawable.h13, R.drawable.h14, R.drawable.h15,
	        R.drawable.h16, R.drawable.h17, R.drawable.h18, R.drawable.h19, R.drawable.h20,
	    	R.drawable.h21, R.drawable.h22, R.drawable.h23, R.drawable.h24, R.drawable.h25,
	        R.drawable.h26, R.drawable.h27, R.drawable.h28, R.drawable.h29, R.drawable.h30,
	        R.drawable.h31, R.drawable.h32, R.drawable.h33, R.drawable.h34, R.drawable.h35,
	        R.drawable.h36, R.drawable.h37, R.drawable.h38, R.drawable.h39, R.drawable.h40,
	        R.drawable.h41, R.drawable.h42, R.drawable.h43, R.drawable.h44, R.drawable.h45,
        };
	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
			if (convertView == null) {
				imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(125, 125));
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				imageView.setPadding(8, 8, 8, 8);
			} else {
				imageView = (ImageView) convertView;
			}
		imageView.setImageResource(mThumbIds[position]);
		return imageView;
	    }
}