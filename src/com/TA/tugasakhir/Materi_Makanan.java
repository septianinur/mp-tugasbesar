package com.TA.tugasakhir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class Materi_Makanan extends Activity implements OnClickListener, OnItemClickListener {
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.materi_isi);
        GridView gridView = (GridView) findViewById(R.id.gridview_followed);
        gridView.setAdapter(new a_makanan(this));
        gridView.setOnItemClickListener(this);
                
        TextView t1 = (TextView) findViewById(R.id.penjelasan);
        String a = "POLA KALIMAT \n jika dalam bentuk positif maka Pola Kalimatnya adalah\n" +
        		"(Nama Makanan) o (Bentuk Masu) \njika dalam bentuk negatif maka Pola Kalimatnya adalah\n" +
        		"(Nama Makanan) o (Bentuk Masu) masen \nContoh : \n" +
        		" (+) Pan o Tabemasu (Makan Roti)\n (-) Pan o Tabemasen (Tidak Makan Roti)";
        t1.setText(a);
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent i = new Intent(this, detail_image_makanan.class);
        Bundle b = new Bundle();
        b.putInt("posisi", position);
        i.putExtras(b);
        startActivity(i);
	}

	@Override
	public void onClick(View arg0) {}
}