package com.TA.tugasakhir;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;

public class MainActivity extends Activity {
	private static int splashInterval = 1500;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run(){
				Intent i = new Intent(MainActivity.this, splashscreen.class);
				startActivity(i);
				finish();
			}
		}, splashInterval);
	}
}