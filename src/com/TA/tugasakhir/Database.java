package com.TA.tugasakhir;

import java.io.IOException;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.*;

public class Database extends SQLiteOpenHelper{
	
	SQLiteDatabase db = null;
	
	private static String DB_NAME = "nihon.db";

	public Database(Context context) {
		super(context, DB_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table cakap(_id integer primary key,judul text," +
				"subjud1 text,isi1 text,isi2 text," +
				"subjud2 text,isi3 text,isi4 text," +
				"subjud3 text,isi5 text,isi6 text," +
				"subjud4 text,isi7 text,isi8 text," +
				"subjud5 text,isi9 text,isi10 text)");	
	}
	
	public Cursor get(String query){
		return(getReadableDatabase().rawQuery(query, null));
	}
	public void exec(String query) throws  IOException{
		getWritableDatabase().execSQL(query);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
	
	}
}