package com.TA.tugasakhir;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

public class menu extends Activity{
	Button b1, b2, b3, b4;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		
	b1	=	(Button)findViewById(R.id.button1);
	b2	=	(Button)findViewById(R.id.button2);
	b3	= 	(Button)findViewById(R.id.button3);
	b4	=	(Button)findViewById(R.id.button4);
	
	b1.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			Intent i = new Intent(getApplicationContext(), menu_materi.class);
			startActivity(i);
		}
	});
	
	b2.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Intent i = new Intent(getApplicationContext(), Main_Activity.class);
			startActivity(i);
			
		}
	});
	
	b3.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			Intent i = new Intent(getApplicationContext(), tentang.class);
			startActivity(i);
			
		}
	});
	
	b4.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			exit();
		}
	});
}

	public void exit(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Yakin akan keluar?")
		.setCancelable(false)
		.setPositiveButton("Ya", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				menu.this.finish();
			}
		})
		.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.cancel();
			}
		}).show();
	
	}

	public boolean onKeyDown(int keyCode, KeyEvent event){
		if (keyCode == KeyEvent.KEYCODE_BACK){
			exit();
		}
	return super.onKeyDown(keyCode, event);
}
}
