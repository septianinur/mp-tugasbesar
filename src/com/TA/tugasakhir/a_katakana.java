package com.TA.tugasakhir;

import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class a_katakana extends BaseAdapter{
Context mContext;
	
	public a_katakana(Context c)
    {
        mContext = c;
    }
 
    public static Integer[] mThumbIds =
        {
	    	R.drawable.k1, R.drawable.k2, R.drawable.k3, R.drawable.k4, R.drawable.k5,
	        R.drawable.k6, R.drawable.k7,R.drawable.k8, R.drawable.k9, R.drawable.k10,
	    	R.drawable.k11, R.drawable.k12, R.drawable.k13, R.drawable.k14, R.drawable.k15,
	        R.drawable.k16, R.drawable.k17, R.drawable.k18, R.drawable.k19, R.drawable.k20,
	    	R.drawable.k21, R.drawable.k22, R.drawable.k23, R.drawable.k24, R.drawable.k25,
	        R.drawable.k26, R.drawable.k27, R.drawable.k28, R.drawable.k29, R.drawable.k30,
	        R.drawable.k31, R.drawable.k32, R.drawable.k33, R.drawable.k34, R.drawable.k35,
	        R.drawable.k36, R.drawable.k37, R.drawable.k38, R.drawable.k39, R.drawable.k40,
	        R.drawable.k41, R.drawable.k42, R.drawable.k43, R.drawable.k44, R.drawable.k45,
        };
	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
			if (convertView == null) {
				imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(125, 125));
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				imageView.setPadding(8, 8, 8, 8);
			} else {
				imageView = (ImageView) convertView;
			}
		imageView.setImageResource(mThumbIds[position]);
		return imageView;
	    }
}