package com.TA.tugasakhir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class Materi_BB extends Activity implements OnClickListener, OnItemClickListener {
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.materi_isi);
        GridView gridView = (GridView) findViewById(R.id.gridview_followed);
        gridView.setAdapter(new a_bb(this));
        gridView.setOnItemClickListener(this);
        
        TextView t1 = (TextView) findViewById(R.id.penjelasan);
        String a = "POLA KALIMAT 1 menyatakan keberadaan benda\n" +
        		"(Nama Tempat) No (Posisi) Ni (Nama Barang) ga Arimasu\n" +
        		"Contoh : \nkyousitsu no naka ni hon ga arimasu (ada buku di dalam kelas)" +
        		"POLA KALIMAT 2  menyatakan keberadaan benda \n" +
        		"(Nama Benda) wa (Nama Tempat) no (Posisi) Ni Arimasu\n" +
        		"Contoh : \nHon wa tsukue no sita ni arimasu ( buku berada dibawah meja)";
        t1.setText(a);
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent i = new Intent(this, detail_image_bb.class);
        Bundle b = new Bundle();
        b.putInt("posisi", position);
        i.putExtras(b);
        startActivity(i);
	}

	@Override
	public void onClick(View arg0) {}
}