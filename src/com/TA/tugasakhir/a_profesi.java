package com.TA.tugasakhir;

import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class a_profesi extends BaseAdapter{
Context mContext;
	
	public a_profesi(Context c)
    {
        mContext = c;
    }
 
    public static Integer[] mThumbIds =
        {
	    	R.drawable.profesi1, R.drawable.profesi2, R.drawable.profesi3, R.drawable.profesi4, R.drawable.profesi5,
	        R.drawable.profesi6, R.drawable.profesi7,R.drawable.profesi8, R.drawable.profesi9, R.drawable.profesi10,
	    	R.drawable.profesi11, R.drawable.profesi12
        };
	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
			if (convertView == null) {
				imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(125, 125));
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				imageView.setPadding(8, 8, 8, 8);
			} else {
				imageView = (ImageView) convertView;
			}
		imageView.setImageResource(mThumbIds[position]);
		return imageView;
	    }
}