package com.TA.tugasakhir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class splashscreen extends Activity{

	private static int splashInterval = 1500;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run(){
				Intent i = new Intent(splashscreen.this, menu.class);
				
				startActivity(i);
				finish();
			}
		}, splashInterval);
	}
}
