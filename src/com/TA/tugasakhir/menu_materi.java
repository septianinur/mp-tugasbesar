package com.TA.tugasakhir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class menu_materi extends Activity{
TextView t1,t2 ,t3 ,t4 ,t5 ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_materi);
		
		t1	=	(TextView)findViewById(R.id.m_huruf);
		t2	=	(TextView)findViewById(R.id.m_makanan);
		t3	=	(TextView)findViewById(R.id.m_profesi);
		t4	=	(TextView)findViewById(R.id.m_bb);
		t5	=	(TextView)findViewById(R.id.m_transportasi);
		
		t1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getApplicationContext(), Menu_huruf.class);
				startActivity(i);
			}
		});
		
		t2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getApplicationContext(), Materi_Makanan.class);
				startActivity(i);
			}
		});

		t3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getApplicationContext(), Materi_Profesi.class);
				startActivity(i);
			}
		});

		t4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getApplicationContext(), Materi_BB.class);
				startActivity(i);
			}
		});

		t5.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(getApplicationContext(), Materi_transportasi.class);
				startActivity(i);
			}
		});
}
}