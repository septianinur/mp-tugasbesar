package com.TA.tugasakhir;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class Materi_transportasi extends Activity implements OnClickListener, OnItemClickListener {
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.materi_isi);
        GridView gridView = (GridView) findViewById(R.id.gridview_followed);
        gridView.setAdapter(new a_transportasi(this));
        gridView.setOnItemClickListener(this);
        
        TextView t1 = (TextView) findViewById(R.id.penjelasan);
        String a = "POLA KALIMAT 1 \n" +
        		"(Alat Transportasi) de (Nama Tempat) e ikimasu/kimasu/kaerimasu \n" +
        		"Pola Kalimat ini untuk menyatakan pergi, datang dan pulang dengan menggunakan suatu alat transportasi\n" +
        		"'de' berarti dengan menggunakan alat\n" +
        		"Contoh :\n" +
        		"Basu de gakkou e ikimasu (Pergi ke sekolah naik bus)\n" +
        		"\nPOLA KALIMAT 2 \n" +
        		"(Nama Tempat1) kara (Nama Tempat2) made (Alat Transportasi) de (Jangka Waktu) gurai desu \n" +
        		"Pola Kalimat ini digunakan untuk menyatakan jangka waktu yang dibutuhkan untuk dari suatu tempat ke tempat lain\n" +
        		"'kara' berarti dari\n" +
        		"'made' berarti sampai\n" +
        		"'gurai' berarti kira kira\n" +
        		"Contoh :\n" +
        		"Uchi kara gakkou made basu de sanjuppun gurai desu\n" +
        		"(Dari rumah ke sekolah dengan bus membutuhkan waktu kira kira 30 menit";
        t1.setText(a);
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent i = new Intent(this, detail_image_transport.class);
        Bundle b = new Bundle();
        b.putInt("posisi", position);
        i.putExtras(b);
        startActivity(i);
	}

	@Override
	public void onClick(View arg0) {}
}